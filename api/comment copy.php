<?php 
	session_start();
// Connect to the database
$mysql_host = "localhost";
$mysql_database = "drugs"; //create the database called "comment_sys"
$mysql_user = "drugs";
$mysql_password = "drugs";

mysql_connect($mysql_host,$mysql_user,$mysql_password);
mysql_select_db($mysql_database); 
$id_post = "1"; //the post or the page id

require '../signup-email-verification/class.user.php';
$user_home = new USER();


$stmt = $user_home->runQuery("SELECT * FROM tbl_users WHERE userID=:uid");
$stmt->execute(array(":uid"=>$_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC); 
?>
<div class="col-lg-12 col-md-12 content-item" style="padding-bottom:30px;padding-top: 20px;"> 
    <?php 
        if($_SESSION['userSession'] == true){
	        echo '<a class="external" href="/drugs/signup-email-verification/logout.php" style="float:right;padding-left:10px">Logout</a><span style="float:right;padding-left: 20px;">Welcome '.$row['userName'].'!</span>';
	    }else{
		    echo "<a class='external' href='/drugs/signup-email-verification/mobile-index.php' data-transition='slide' style='float:right;padding-left:10px'>Login</a>";
	    }
        
    ?>
    
<!--
    <form action="search.php" method="GET" style="float:right;">
        <input type="text" name="query" />
        <input type="submit" value="Search" />
    </form>	
-->	        
</div>	
<div class="cmt-container" >
    <?php 
    $sql = mysql_query("SELECT * FROM comments WHERE id_post = '$id_post'") or die(mysql_error());;
    while($affcom = mysql_fetch_assoc($sql)){ 
        $name = $affcom['name'];
        $email = $affcom['email'];
        $comment = $affcom['comment'];
        $date = $affcom['date'];

        // Get gravatar Image 
        // https://fr.gravatar.com/site/implement/images/php/
        $default = "mm";
        $size = 35;
        $grav_url = "http://www.gravatar.com/avatar/".md5(strtolower(trim($email)))."?d=".$default."&s=".$size;

    ?>
    <div class="cmt-cnt">
        <img src="<?php echo $grav_url; ?>" />
        <div class="thecom">
            <h5><?php echo $name; ?></h5><span data-utime="1371248446" class="com-dt"><?php echo $date; ?></span>
            <br/>
            <p>
                <?php echo $comment; ?>
            </p>
        </div>
    </div><!-- end "cmt-cnt" -->
    <?php } ?>
                    <?php 
                      if($_SESSION['userSession'] == true){
                      //your logout link
                        echo "<div class='new-com-bt'><span>Write a comment ...</span></div>";
                        echo "<div class='new-com-cnt'><input type='text' id='name-com' name='name-com' value='".$row["userName"]."' placeholder='".$row["userName"]."' disabled/>";
                        echo "<input type='text' id='mail-com' name='mail-com' value='".$row["userEmail"]."' placeholder='".$row["userEmail"]."' disabled/>";
                        echo "<textarea class='the-new-com'></textarea>";
                        echo "<div class='bt-add-com'>Post comment</div>";
                        echo "<div class='bt-cancel-com'>Cancel</div></div>";

                      }else{
                      //your login link
                      	echo "<p>Please Login to Leave Comments.</p>";
                        echo "<a data-transition='slide' href='/drugs/signup-email-verification/mobile-index.php'>Login</a>";
                      }
                    ?> 


<!--
    <div class="new-com-cnt">
        <input type="text" id="name-com" name="name-com" value="" placeholder="Your name" />
        <input type="text" id="mail-com" name="mail-com" value="" placeholder="Your e-mail adress" />
        <textarea class="the-new-com"></textarea>
        <div class="bt-add-com">Post comment</div>
        <div class="bt-cancel-com">Cancel</div>
    </div>
-->
    <div class="clear"></div>
</div><!-- end of comments container "cmt-container" -->

<script type="text/javascript">
   $(function(){ 
        //alert(event.timeStamp);
        $('.new-com-bt').click(function(event){    
            $(this).hide();
            $('.new-com-cnt').show();
            $('#name-com').focus();
        });

        /* when start writing the comment activate the "add" button */
        $('.the-new-com').bind('input propertychange', function() {
           $(".bt-add-com").css({opacity:0.6});
           var checklength = $(this).val().length;
           if(checklength){ $(".bt-add-com").css({opacity:1}); }
        });

        /* on clic  on the cancel button */
        $('.bt-cancel-com').click(function(){
            $('.the-new-com').val('');
            $('.new-com-cnt').fadeOut('fast', function(){
                $('.new-com-bt').fadeIn('fast');
            });
        });

        // on post comment click 
        $('.bt-add-com').click(function(){
            var theCom = $('.the-new-com');
            var theName = $('#name-com');
            var theMail = $('#mail-com');

            if( !theCom.val()){ 
                alert('You need to write a comment!'); 
            }else{ 
                $.ajax({
                    type: "POST",
                    url: "ajax/add-comment.php",
                    data: 'act=add-com&id_post='+<?php echo $id_post; ?>+'&name='+theName.val()+'&email='+theMail.val()+'&comment='+theCom.val(),
                    success: function(html){
                        theCom.val('');
                        theMail.val('');
                        theName.val('');
                        $('.new-com-cnt').hide('fast', function(){
                            $('.new-com-bt').show('fast');
                            $('.new-com-bt').before(html);  
                        })
                    }  
                });
            }
        });

    });
</script>    