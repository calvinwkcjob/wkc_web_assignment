<?php
session_start();
require_once 'class.user.php';
$user_login = new USER();

if($user_login->is_logged_in()!="")
{
	$user_login->redirect('/drugs/mobile/');
}

if(isset($_POST['btn-login']))
{
	$email = trim($_POST['txtemail']);
	$upass = trim($_POST['txtupass']);
	
	if($user_login->login($email,$upass))
	{
		$user_login->redirect('/drugs/mobile/');
	}
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Login</title>
    <!-- Bootstrap -->
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="../css/jquery.mobile.structure-1.0.1.css" />
<!--
	<link rel="apple-touch-icon" href="images/launch_icon_57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/launch_icon_72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/launch_icon_114.png" />
-->
	<link rel="stylesheet" href="/drugs/css/jquery.mobile-1.4.5.min.css" />
	<link rel="stylesheet" href="/drugs/css/custom.css" />
	<script src="/drugs/js/jquery-1.7.1.min.js"></script>
	<script src="/drugs/js/jquery.mobile-1.4.5.min.js"></script>

     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body id="login"> 
<div id="choisir_ville" data-role="page">
	
	<div data-role="header" data-add-back-btn="true"> 
		<h1>Drugs Details</h1>
	</div> 
 
	<div data-role="content">
				<?php 
				if(isset($_GET['inactive']))
				{
					?>
		            <div class='alert alert-error'>
						<button class='close' data-dismiss='alert'>&times;</button>
						<strong>Sorry!</strong> This Account is not Activated Go to your Inbox and Activate it. 
					</div>
		            <?php
				}
				?>
		        <form class="form-signin" method="post">
		        <?php
		        if(isset($_GET['error']))
				{
					?>
		            <div class='alert alert-success'>
						<button class='close' data-dismiss='alert'>&times;</button>
						<strong>Wrong Details!</strong> 
					</div>
		            <?php
				}
				?>
		        <h2 class="main-title text-center dark-blue-text">Sign In.</h2><hr />
		        <input type="email" class="input-block-level" placeholder="Email address" name="txtemail" required />
		        <input type="password" class="input-block-level" placeholder="Password" name="txtupass" required />
		     	<hr />
		        <button class="btn btn-large btn-primary" type="submit" name="btn-login">Sign in</button>
		        <a data-transition="slide" href="mobile-signup.php" style="float:right;" class="btn btn-large">Sign Up</a><hr />
		        <a data-transition="slide" href="mobile-fpass.php">Lost your Password ? </a>
		      </form>	
	</div>
</div>  
  
    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
<?php include("../include/footer.php"); ?>    
  </body>
</html>